{
    let fields = Array.prototype.slice.call(document.getElementById('fields').children);
    let popUp = document.getElementsByClassName('popup');
    const saveMobile = document.getElementById('save');
    const cancelMobile = document.getElementById('cancel');
    const mobPopUp = document.getElementById('show-popup');

    fields.forEach((elem) => {
        let index = fields.indexOf(elem);
        let edit = document.getElementsByClassName('edit')[index];
        let save = document.getElementsByClassName('save')[index];
        let cancel = document.getElementsByClassName('cancel')[index];


        let inputMobile = document.getElementsByClassName('input')[index];
        let inputMobileVal = document.getElementsByClassName('input')[index].value;

        edit.addEventListener('click', (evt) => {
            evt.stopPropagation();
            let displayed = document.querySelectorAll('.display');
            Array.from(displayed).forEach(function(item) {

                item.classList.remove('display');
            });

            popUp[index].classList.add("display");
        });

        save.addEventListener('click',  (evt) => {
            evt.stopPropagation();
            let inputVal = document.getElementsByTagName('input')[index].value;
            if (inputVal !== '') {
                document.getElementsByClassName('content')[index].innerText = inputVal;
                popUp[index].classList.remove("display");
            }
            else {
                alert("You can't let the field empty")
            }
        });

        cancel.addEventListener('click', (evt) => {
            evt.stopPropagation();
            popUp[index].classList.remove("display");
        });

        inputMobile.addEventListener('change',(evt) => {
            evt.stopPropagation();
            inputMobileVal = inputMobile.value;
        });

        saveMobile.addEventListener('click',  (evt) => {
            let inputLastName = document.getElementById('input-Name');
            let inputFirstName = document.getElementById('input-name');

            evt.stopPropagation();
            if (inputMobileVal !== '') {
                if(inputMobile.id === inputFirstName.id) {
                    document.getElementsByClassName('content')[index].innerText = inputFirstName.value + inputLastName.value;
                    mobPopUp.classList.remove("display");
                }
                else {
                    document.getElementsByClassName('content')[index].innerText = inputMobileVal;
                    mobPopUp.classList.remove("display");
                }
            }
            else {
                alert("You can't let the field empty");
            }
        });

        cancelMobile.addEventListener('click',  (evt) => {
            evt.stopPropagation();
            if (inputMobileVal !== '') {
                mobPopUp.classList.remove("display");
            }
            else {
                alert("You can't let the field empty");
            }
        });
    });

    let showPopUp = () => {
        document.getElementById('show-popup').classList.add("display");
    };

    document.getElementById('mob-edit').addEventListener('click', showPopUp, false);
}