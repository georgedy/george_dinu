{
    let tabItems = {
        title: {
            "type": "name",
            "content": "Name",
            "fname": "First Name",
            "fNameVal": "Jessica",
            "lname": "Last Name",
            "lNameVal": "Parker",
            "value": ""
        },
        website: {
            "type": "website",
            "content": "Website",
            "value": "www.seller.com",
            "fname": "",
            "fNameVal": "",
            "lname": "",
            "lNameVal": "",
        },
        phone: {
            "type": "phone",
            "content": "Phone",
            "value": "(949) 325 - 68594",
            "fname": "",
            "fNameVal": "",
            "lname": "",
            "lNameVal": "",
        },
        address: {
            "type": "address",
            "content": "CITY, STATE AND ZIP",
            "value": "Newport Beach, CA",
            "fname": "",
            "fNameVal": "",
            "lname": "",
            "lNameVal": "",
        }

    };

    let menuItems = {
        about: {
            "name": "about",
            "url": "about",
            "active": "active",
        },

        settings: {
            "name": "settings",
            "url": "settings",
            "active": "",
        },

        option1: {
            "name": "option1",
            "url": "option1",
            "active": "",
        },
        option2: {
            "name": "option2",
            "url": "option2",
            "active": "",
        },
        option3: {
            "name": "option3",
            "url": "option3",
            "active": "",
        },
    };

    const templateList = document.getElementById("template-list-item");
    const templateTabs = document.getElementById("template-tabs");
    const templateContent = document.getElementById("template-content");
    const templateMobile = document.getElementById("template-mobile");
// Get the contents of the template
    let templateListHtml = templateList.innerHTML;
    let templateTabsHtml = templateTabs.innerHTML;
    let templateContentHtml = templateContent.innerHTML;
    let templateMobileHtml = templateMobile.innerHTML;
// Final HTML variable as empty string
    let listHtml = "";
    let tabsHtml = "";
    let itemsHtml = "";
    let mobileHtml = "";

    for (let key in menuItems) {
        listHtml += templateListHtml.replace(/{{id}}/g, menuItems[key]["id"])
            .replace(/{{name}}/g, menuItems[key]["name"])
            .replace(/{{active}}/g, menuItems[key]["active"])
            .replace(/{{content}}/g, menuItems[key]["content"])
            .replace(/{{url}}/g, menuItems[key]["url"]);
    }

    for (let key in menuItems) {
        tabsHtml += templateTabsHtml.replace(/{{id}}/g, menuItems[key]["id"])
            .replace(/{{name}}/g, menuItems[key]["name"])
            .replace(/{{active}}/g, menuItems[key]["active"])
            .replace(/{{url}}/g, menuItems[key]["url"]);
    }

    for (let key in tabItems) {
        itemsHtml += templateContentHtml.replace(/{{id}}/g, tabItems[key]["id"])
            .replace(/{{type}}/g, tabItems[key]["type"])
            .replace(/{{fname}}/g, tabItems[key]["fname"])
            .replace(/{{fnameval}}/g, tabItems[key]["fNameVal"])
            .replace(/{{lname}}/g, tabItems[key]["lname"])
            .replace(/{{lnameval}}/g, tabItems[key]["lNameVal"])
            .replace(/{{value}}/g, tabItems[key]["value"])
            .replace(/{{content}}/g, tabItems[key]["content"]);
    }


    for (let key in tabItems) {
        mobileHtml += templateMobileHtml.replace(/{{id}}/g, tabItems[key]["id"])
            .replace(/{{type}}/g, tabItems[key]["type"])
            .replace(/{{fname}}/g, tabItems[key]["fname"])
            .replace(/{{fnameval}}/g, tabItems[key]["fNameVal"])
            .replace(/{{lname}}/g, tabItems[key]["lname"])
            .replace(/{{lnameval}}/g, tabItems[key]["lNameVal"])
            .replace(/{{value}}/g, tabItems[key]["value"])
            .replace(/{{content}}/g, tabItems[key]["content"]);

    }


// Replace the HTML of #list with final HTML
    document.getElementById("nav-tabs").innerHTML = listHtml;
    document.getElementById("tabs-content").innerHTML = tabsHtml;
    document.getElementById("fields").innerHTML = itemsHtml;
    document.getElementById("mobile-popup").innerHTML = mobileHtml;
}