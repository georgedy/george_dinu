{
    let onTabClick = (event) => {
    event.preventDefault();

    let active = document.querySelectorAll('.active');

    Array.from(active).forEach(function(item) {
        item.classList.remove('active');
    });

    // activate new tab and panel
    event.target.parentElement.classList.add('active');
    document.getElementById(event.target.href.split('#')[1]).classList.add('active');
};

    Array.from(document.querySelectorAll('li')).forEach(function(item) {
        item.addEventListener('click', onTabClick, false);
    });
}