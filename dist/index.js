"use strict";

{
    var tabItems = {
        title: {
            "type": "name",
            "content": "Name",
            "fname": "First Name",
            "fNameVal": "Jessica",
            "lname": "Last Name",
            "lNameVal": "Parker",
            "value": ""
        },
        website: {
            "type": "website",
            "content": "Website",
            "value": "www.seller.com",
            "fname": "",
            "fNameVal": "",
            "lname": "",
            "lNameVal": ""
        },
        phone: {
            "type": "phone",
            "content": "Phone",
            "value": "(949) 325 - 68594",
            "fname": "",
            "fNameVal": "",
            "lname": "",
            "lNameVal": ""
        },
        address: {
            "type": "address",
            "content": "CITY, STATE AND ZIP",
            "value": "Newport Beach, CA",
            "fname": "",
            "fNameVal": "",
            "lname": "",
            "lNameVal": ""
        }

    };

    var menuItems = {
        about: {
            "name": "about",
            "url": "about",
            "active": "active"
        },

        settings: {
            "name": "settings",
            "url": "settings",
            "active": ""
        },

        option1: {
            "name": "option1",
            "url": "option1",
            "active": ""
        },
        option2: {
            "name": "option2",
            "url": "option2",
            "active": ""
        },
        option3: {
            "name": "option3",
            "url": "option3",
            "active": ""
        }
    };

    var templateList = document.getElementById("template-list-item");
    var templateTabs = document.getElementById("template-tabs");
    var templateContent = document.getElementById("template-content");
    var templateMobile = document.getElementById("template-mobile");
    // Get the contents of the template
    var templateListHtml = templateList.innerHTML;
    var templateTabsHtml = templateTabs.innerHTML;
    var templateContentHtml = templateContent.innerHTML;
    var templateMobileHtml = templateMobile.innerHTML;
    // Final HTML variable as empty string
    var listHtml = "";
    var tabsHtml = "";
    var itemsHtml = "";
    var mobileHtml = "";

    for (var key in menuItems) {
        listHtml += templateListHtml.replace(/{{id}}/g, menuItems[key]["id"]).replace(/{{name}}/g, menuItems[key]["name"]).replace(/{{active}}/g, menuItems[key]["active"]).replace(/{{content}}/g, menuItems[key]["content"]).replace(/{{url}}/g, menuItems[key]["url"]);
    }

    for (var _key in menuItems) {
        tabsHtml += templateTabsHtml.replace(/{{id}}/g, menuItems[_key]["id"]).replace(/{{name}}/g, menuItems[_key]["name"]).replace(/{{active}}/g, menuItems[_key]["active"]).replace(/{{url}}/g, menuItems[_key]["url"]);
    }

    for (var _key2 in tabItems) {
        itemsHtml += templateContentHtml.replace(/{{id}}/g, tabItems[_key2]["id"]).replace(/{{type}}/g, tabItems[_key2]["type"]).replace(/{{fname}}/g, tabItems[_key2]["fname"]).replace(/{{fnameval}}/g, tabItems[_key2]["fNameVal"]).replace(/{{lname}}/g, tabItems[_key2]["lname"]).replace(/{{lnameval}}/g, tabItems[_key2]["lNameVal"]).replace(/{{value}}/g, tabItems[_key2]["value"]).replace(/{{content}}/g, tabItems[_key2]["content"]);
    }

    for (var _key3 in tabItems) {
        mobileHtml += templateMobileHtml.replace(/{{id}}/g, tabItems[_key3]["id"]).replace(/{{type}}/g, tabItems[_key3]["type"]).replace(/{{fname}}/g, tabItems[_key3]["fname"]).replace(/{{fnameval}}/g, tabItems[_key3]["fNameVal"]).replace(/{{lname}}/g, tabItems[_key3]["lname"]).replace(/{{lnameval}}/g, tabItems[_key3]["lNameVal"]).replace(/{{value}}/g, tabItems[_key3]["value"]).replace(/{{content}}/g, tabItems[_key3]["content"]);
    }

    // Replace the HTML of #list with final HTML
    document.getElementById("nav-tabs").innerHTML = listHtml;
    document.getElementById("tabs-content").innerHTML = tabsHtml;
    document.getElementById("fields").innerHTML = itemsHtml;
    document.getElementById("mobile-popup").innerHTML = mobileHtml;
}
'use strict';

{
    var fields = Array.prototype.slice.call(document.getElementById('fields').children);
    var popUp = document.getElementsByClassName('popup');
    var saveMobile = document.getElementById('save');
    var cancelMobile = document.getElementById('cancel');
    var mobPopUp = document.getElementById('show-popup');

    fields.forEach(function (elem) {
        var index = fields.indexOf(elem);
        var edit = document.getElementsByClassName('edit')[index];
        var save = document.getElementsByClassName('save')[index];
        var cancel = document.getElementsByClassName('cancel')[index];

        var inputMobile = document.getElementsByClassName('input')[index];
        var inputMobileVal = document.getElementsByClassName('input')[index].value;

        edit.addEventListener('click', function (evt) {
            evt.stopPropagation();
            var displayed = document.querySelectorAll('.display');
            Array.from(displayed).forEach(function (item) {

                item.classList.remove('display');
            });

            popUp[index].classList.add("display");
        });

        save.addEventListener('click', function (evt) {
            evt.stopPropagation();
            var inputVal = document.getElementsByTagName('input')[index].value;
            if (inputVal !== '') {
                document.getElementsByClassName('content')[index].innerText = inputVal;
                popUp[index].classList.remove("display");
            } else {
                alert("You can't let the field empty");
            }
        });

        cancel.addEventListener('click', function (evt) {
            evt.stopPropagation();
            popUp[index].classList.remove("display");
        });

        inputMobile.addEventListener('change', function (evt) {
            evt.stopPropagation();
            inputMobileVal = inputMobile.value;
        });

        saveMobile.addEventListener('click', function (evt) {
            var inputLastName = document.getElementById('input-Name');
            var inputFirstName = document.getElementById('input-name');

            evt.stopPropagation();
            if (inputMobileVal !== '') {
                if (inputMobile.id === inputFirstName.id) {
                    document.getElementsByClassName('content')[index].innerText = inputFirstName.value + inputLastName.value;
                    mobPopUp.classList.remove("display");
                } else {
                    document.getElementsByClassName('content')[index].innerText = inputMobileVal;
                    mobPopUp.classList.remove("display");
                }
            } else {
                alert("You can't let the field empty");
            }
        });

        cancelMobile.addEventListener('click', function (evt) {
            evt.stopPropagation();
            if (inputMobileVal !== '') {
                mobPopUp.classList.remove("display");
            } else {
                alert("You can't let the field empty");
            }
        });
    });

    var showPopUp = function showPopUp() {
        document.getElementById('show-popup').classList.add("display");
    };

    document.getElementById('mob-edit').addEventListener('click', showPopUp, false);
}
'use strict';

{
    var onTabClick = function onTabClick(event) {
        event.preventDefault();

        var active = document.querySelectorAll('.active');

        Array.from(active).forEach(function (item) {
            item.classList.remove('active');
        });

        // activate new tab and panel
        event.target.parentElement.classList.add('active');
        document.getElementById(event.target.href.split('#')[1]).classList.add('active');
    };

    Array.from(document.querySelectorAll('li')).forEach(function (item) {
        item.addEventListener('click', onTabClick, false);
    });
}
